package com.example.android.kthoroscope.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import com.example.android.kthoroscope.R
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Setup Navigation Controller
        val navController = Navigation.findNavController(this, R.id.nav_host_fragment)

        // Setup bottom nav. view
        setupBottomNavView(navController)

    }

    private fun setupBottomNavView(navController: NavController) {
        bottom_nav_view.let {
            NavigationUI.setupWithNavController(it, navController)
        }
    }



}
