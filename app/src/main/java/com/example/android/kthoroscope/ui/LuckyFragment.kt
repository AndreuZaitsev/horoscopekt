package com.example.android.kthoroscope.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.isVisible
import androidx.core.view.updateLayoutParams
import androidx.fragment.app.Fragment
import com.example.android.kthoroscope.R
import com.example.android.kthoroscope.utils.toggleVisibility
import kotlinx.android.synthetic.main.fragment_lucky.*


class LuckyFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_lucky, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        iv_lucky_number_window.setOnClickListener {
            if (iv_lucky_number_gif.isVisible) showLuckyNumber()
        }
    }

    private fun showLuckyNumber() {
        // Hide gif
        iv_lucky_number_gif.toggleVisibility()
        // Redesign to show the result
        val params = iv_lucky_number_window.layoutParams as ConstraintLayout.LayoutParams
        iv_lucky_number_window.updateLayoutParams { params.setMargins(0, 24, 0, 0) }
        // Set a number and a description
        val number = catchRandomNumber()
        tv_lucky_result_number.text = number.toString()
        tv_lucky_description.text = getDescription(number)
    }

    private fun catchRandomNumber() = (0..100).random()

    private fun getDescription(number: Int): String? {
        val range2 = 2..100
        val range3 = 3..100
        val range4 = 4..100
        val range5 = 5..100
        val range6 = 6..100
        val range7 = 7..100
        val range8 = 8..100
        val range9 = 9..100
        val stepValue = 10

        return when (number) {
            in range2 step stepValue -> context?.getString(R.string.luckyN_desc_2)
            in range3 step stepValue -> context?.getString(R.string.luckyN_desc_3)
            in range4 step stepValue -> context?.getString(R.string.luckyN_desc_4)
            in range5 step stepValue -> context?.getString(R.string.luckyN_desc_5)
            in range6 step stepValue -> context?.getString(R.string.luckyN_desc_6)
            in range7 step stepValue -> context?.getString(R.string.luckyN_desc_7)
            in range8 step stepValue -> context?.getString(R.string.luckyN_desc_8)
            in range9 step stepValue -> context?.getString(R.string.luckyN_desc_9)
            else -> context?.getString(R.string.luckyN_desc_1)
        }
    }

}
