package com.example.android.kthoroscope.utils

import android.content.Context
import android.view.View
import android.widget.Toast
import androidx.fragment.app.FragmentActivity

fun Context.toast(context: Context = this, message: String = "Toast!", duration: Int = Toast.LENGTH_SHORT) =
    Toast.makeText(context, message, duration).show()

fun View.toggleVisibility() =
    when {
        this.visibility == View.VISIBLE -> this.visibility = View.GONE
        this.visibility == View.GONE -> this.visibility = View.VISIBLE
        else -> this.visibility = View.VISIBLE
    }

fun hideSystemUI(activity: FragmentActivity?) {
    // Enables regular immersive mode.
    // For "lean back" mode, remove SYSTEM_UI_FLAG_IMMERSIVE.
    // Or for "sticky immersive," replace it with SYSTEM_UI_FLAG_IMMERSIVE_STICKY
    activity?.window?.decorView?.systemUiVisibility = (
            View.SYSTEM_UI_FLAG_IMMERSIVE
            // Hide the nav bar and status bar
            or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
            or View.SYSTEM_UI_FLAG_FULLSCREEN
            )
}

// Shows the system bars by removing all the flags
// except for the ones that make the content appear under the system bars.
fun showSystemUI(activity: FragmentActivity?) {
    activity?.window?.decorView?.systemUiVisibility = (
            View.SYSTEM_UI_FLAG_VISIBLE
            )
}
