package com.example.android.kthoroscope.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.android.kthoroscope.R
import com.example.android.kthoroscope.data.TarotCard

class TarotAdapter(
    private val tarotCards: MutableList<TarotCard>,
    private val clickListener: (view: View, position: Int, card: TarotCard) -> Unit
) : RecyclerView.Adapter<TarotAdapter.TarotViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TarotViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_tarot_card, parent, false)
        return TarotViewHolder(view)
    }

    fun removeCard(position: Int) {
        tarotCards.removeAt(position)
        notifyItemRemoved(position)
    }

    fun clearCards() {
        tarotCards.removeAll(tarotCards)
        notifyItemRangeRemoved(0, 4)
    }

    override fun getItemCount() = tarotCards.size

    override fun onBindViewHolder(holder: TarotViewHolder, position: Int) {
        val card = tarotCards[position]
        holder.bind(card)
    }


    inner class TarotViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(card: TarotCard) {
            itemView.setOnClickListener { clickListener(itemView, adapterPosition, card) }
        }

    }
}